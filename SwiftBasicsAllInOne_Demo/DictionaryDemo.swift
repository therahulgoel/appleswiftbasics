//
//  DictionaryDemo.swift
//  SwiftBasicsAllInOne_Demo
//
//  Created by RAHUL GOEL on 29/09/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

import Foundation

func updatingNestedDictionaryKeyValuePairs()
{
    var dict:Dictionary<String,AnyObject> = ["key1":"A",
        "key2":"B",
        "key3":["subKey3":"C"] as Dictionary<String,AnyObject>]
    
    var someValue = dict["key3"] as! Dictionary<String,AnyObject>
    
    //To add a new key,value pair (subKey1,"D") !
    someValue["subKey31"] = "D"
    
    //To update the value for the key specified !
    dict.updateValue(someValue, forKey: "key3")
    println("\(dict)")
    
    //To remove the pair based on the key specified !
    dict.removeValueForKey("key1")
    println("\(dict)")
    
    println("Number Of Key Pairs = \(dict.count)")
    
    //Iterating Through All Elements in Dictionary
    for (key,value) in dict
    {
        println("\(key) : \(value)")
    }
    
    //Dictionary is Value Type In Swift (not the reference) So they are copied as value when passed around,
    
    //Checking If the Given Dictionary Is Empty Or not
    println("\(dict.isEmpty)")
    
    
    
    
}