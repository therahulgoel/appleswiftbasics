//: Playground - noun: a place where people can play

import UIKit

var str = "Array PlayGround"

//Below Both are the same
var arr:Array<Int> = Array<Int>()
var array:[Int] = [Int]()

//Creating an array with a Repeted Default Value
var tenDoubles = [Double](count: 10, repeatedValue: 56.0)

//Combining Two arrays of same type in single array
var A = [Double](count: 3, repeatedValue: 4.5)
var B = [Double](count: 3, repeatedValue: 3)
var combinedArray = A + B






